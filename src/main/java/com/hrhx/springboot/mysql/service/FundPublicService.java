package com.hrhx.springboot.mysql.service;

import com.hrhx.springboot.domain.FundPublic;

import java.util.List;

/**
 * 
 * @author duhongming
 *
 */
public interface FundPublicService {
    List<FundPublic> getFundPublic();
}
